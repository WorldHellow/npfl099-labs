1st Lab Assignment -- Dialogue Datasets
=======================================

Your task is to select one dialogue dataset (agree this with us over Slack/Zoom/email, everyone should get a different dataset!), download and explore it.

Find out:
- What kind of data it is (domain, modality)
- How it was collected
- What kind of dialogue system or dialogue system component it's designed for
- What kind of annotation is present (if any at all), how was it obtained (human/automatic)
- What format is it stored in
- What is the license

Here you can use the dataset description/paper that came out with the data. 
The papers are linked from the dataset webpages or from here. If you can't find a paper, ask us and we'll try to help.

Measure:
- Total data length (dialogues, turns, sentences, words)
- Mean/std dev dialogue lengths (dialogues, turns, sentences, words)
- Vocabulary size
- User/ system entropy (or just overall entropy, if no user/system distinction can be made)

Here you should use your own programming skills.

Have a closer look at the data and try to make an impression -- does the data look natural? 
How difficult do you think this dataset will be to learn from? How usable will it be in an actual system?
Do you think there's some kind of problem or limitation with the data?

Write up a short summary detailing all of your findings in Markdown and commit it into the [dataset_descriptions](dataset_descriptions/) directory. Send us your Gitlab username so we can give you commit rights.

**Deadline for selecting a dataset: October 13th**
**Deadline for submitting the report: October 20th**

If you need an extension, we need to agree on it *before the original deadline*.


Datasets to select from
-----------------------

### Primarily:


* CMU Document Grounded Data (https://github.com/festvox/datasets-CMU_DoG) *-- Lorcan*
* Clinc-OOS (https://github.com/clinc/oos-eval)
* ConvBank (https://gitlab.com/ucdavisnlp/dialog-parsing) 
* CrossWOZ (https://github.com/thu-coai/CrossWOZ)
* DSTC2 (http://camdial.org/~mh521/dstc/)
* DailyDialog (http://yanran.li/dailydialog.html) *-- Christián*
* Edina Self-dialogue (https://github.com/jfainberg/self_dialogue_corpus) 
* FB Semantic Parsing for Dialogue (https://www.aclweb.org/anthology/D18-1300/, http://fb.me/semanticparsingdialog) *-- Souro*
* FB multilingual (https://arxiv.org/pdf/1810.13327.pdf, https://fb.me/multilingual_task_oriented_data) 
* Holl-E (https://github.com/nikitacs16/Holl-E) *-- Michael*
* HWU NLU Evaluation data (https://github.com/xliuhw/NLU-Evaluation-Data)
* KG-Copy Data (https://github.com/SmartDataAnalytics/KG-Copy_Network, the `data` subdirectory)
* MS Dialogue Challenge Data (https://github.com/xiul-msr/e2e_dialog_challenge) *-- Vladislav*
* MetaLWOz/DSTC8 (https://www.microsoft.com/en-us/research/project/metalwoz/) 
* Mimic & Rephrase (https://www.aclweb.org/anthology/K19-1037, https://github.com/square/MimicAndRephrase/) *-- Saad*
* MultiWOZ 2.2 (https://www.aclweb.org/anthology/2020.nlp4convai-1.13/, https://github.com/budzianowski/multiwoz)
* PersonaChat (http://convai.io/#personachat-convai2-dataset) *-- Anna*
* PolyAI Task-specific – Banking (https://github.com/PolyAI-LDN/task-specific-datasets) *-- Tomáš S.*
* PolyAI Task-specific – Span Extraction (https://github.com/PolyAI-LDN/task-specific-datasets)
* SPOLIN improv data  (https://github.com/wise-east/spolin) *-- Veronika*
* Schema-Guided Dialogue (https://github.com/google-research-datasets/dstc8-schema-guided-dialogue) *-- Patrícia*
* Snips (https://github.com/snipsco/nlu-benchmark/tree/master/2017-06-custom-intent-engines) *-- František*
* Taskmaster-1 (https://ai.google/tools/datasets/taskmaster-1) *-- Samuel*
* Taskmaster-2 (https://research.google/tools/datasets/taskmaster-2/) *-- Tomáš J.*
* Topical Chat (https://github.com/alexa/Topical-Chat) *-- Dominik*
* Ubuntu Dialogue (https://github.com/rkadlec/ubuntu-ranking-dataset-creator) *-- Dávid*
* Wizard of Wikipedia (https://parl.ai/projects/wizard_of_wikipedia/) *-- Daniel*

### Further dataset ideas:

* ATIS (https://www.aclweb.org/anthology/H90-1021/, https://github.com/howl-anderson/ATIS_dataset/blob/master/README.en-US.md) 
* bAbI (https://research.fb.com/downloads/babi/)
* Cam676 (https://www.aclweb.org/anthology/E17-1042/, https://www.repository.cam.ac.uk/handle/1810/260970) 
* Cambridge in-car SLU (https://ieeexplore.ieee.org/document/6424218, https://aspace.repository.cam.ac.uk/handle/1810/248271)
* DSTC1
* KVRET – Stanford car assistant (https://nlp.stanford.edu/blog/a-new-multi-turn-multi-domain-task-oriented-dialogue-dataset/) 
* More DSTCs
* Maluuba Frames (https://arxiv.org/pdf/1704.00057.pdf, https://www.microsoft.com/en-us/research/project/frames-dataset/) 
* MultiWOZ 2.0 (http://dialogue.mi.eng.cam.ac.uk/index.php/corpus/)


Dataset assignment
------------------

| **Name** | **Dataset** |
|---------------------|:-:|
| Anna   | PersonaChat |
| Daniel | Wizard of Wikipedia    |
| Dávid | Ubuntu Dialogue |
| Dominik | Topical Chat |
| František | Snips |
| Christián | DailyDialog |
| Lorcan | CMU Document Grounded Data |
| Michael | Holl-E |
| Patrícia | Schema-Guided Dialogue |
| Saad | Mimic & Rephrase |
| Samuel | Taskmaster-1 |
| Souro | FB Semantic Parsing for Dialogue |
| Tomáš J. | Taskmaster-2 |
| Tomáš S. | PolyAI Task-Specific – Banking |
| Veronika | SPOLIN |
| Vladislav | MS Dialogue Challenge |



Further Links
-------------

Dataset surveys (broader, but shallower than what we're aiming at):
- https://breakend.github.io/DialogDatasets/ 
- https://github.com/AtmaHou/Task-Oriented-Dialogue-Dataset-Survey
